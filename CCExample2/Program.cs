﻿using System;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace CCExample2
{
  public static class Program
  {
    public static void Main(string[] args)
    {
      Console.WriteLine("CCExample2 started");
      CreateWebHostBuilder(args).Build().Run();
      Console.WriteLine("CCExample2 finished");
    }

    public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
        WebHost.CreateDefaultBuilder(args)
            .UseStartup<Startup>();
  }
}
