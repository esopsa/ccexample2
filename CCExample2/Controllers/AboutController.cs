﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CCExample2.Controllers
{
  public class About
  {
    [JsonProperty("name")]
    public string Name = Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName);
    [JsonProperty("version")]
    public string Ver = "v1.2.3";
    [JsonProperty("framework")]
    public string Fw = RuntimeInformation.FrameworkDescription;
    [JsonProperty("os")]
    public string Os = RuntimeInformation.OSDescription;
  }

  [Route("[controller]")]
  [ApiController]
  public class AboutController: ControllerBase
  {
    [HttpGet]
    public JsonResult Get()
    {
      Console.WriteLine("About");
      JsonSerializerSettings jss = new JsonSerializerSettings { Formatting = Formatting.Indented };
      JsonResult jr = new JsonResult(new About(), jss) { StatusCode = StatusCodes.Status200OK };
      return jr;
    }
  }
}