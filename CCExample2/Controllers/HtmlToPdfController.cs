﻿using System;
using Aspose.Html.Cloud.Sdk.Api;
using Aspose.Html.Cloud.Sdk.Api.Model;
using Microsoft.AspNetCore.Mvc;

namespace CCExample2.Controllers
{
  [Route("[controller]")]
  [ApiController]
  public class HtmlToPdfController: ControllerBase
  {
    [HttpGet]
    public FileStreamResult Get()
    {
      Console.WriteLine("HtmlToPdf");
      string jwt = Helpers.GetJWT(Request);
      string basePath = "https://api-qa.aspose.cloud";
      HtmlApi ha = new HtmlApi(jwt, basePath);
      string tmpPdf = Guid.NewGuid().ToString("N");
      Console.WriteLine("HtmlApi.PostConvertDocumentToPdf");
      ha.PostConvertDocumentToPdf("HTMLToPDF.html", tmpPdf);
      StorageApi sa = new StorageApi(jwt, basePath);
      Console.WriteLine("StorageApi.DownloadFile");
      StreamResponse spdf = sa.DownloadFile(tmpPdf);
      Console.WriteLine("StorageApi.DeleteFile");
      sa.DeleteFile(tmpPdf);
      return new FileStreamResult(spdf.ContentStream, "application/octet-stream");
    }
  }
}
