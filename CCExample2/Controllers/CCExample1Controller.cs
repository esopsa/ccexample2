﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;

namespace CCExample2.Controllers
{
  [Route("[controller]")]
  [ApiController]
  public class CCExample1Controller: ControllerBase
  {
    [HttpGet]
    public FileStreamResult Get()
    {
      string jwt = Helpers.GetJWT(Request);
      HttpWebRequest rq = WebRequest.CreateHttp(new Uri("http://app8.189186.gw.k8s.saltov.dynabic.com/version"));
      rq.Method = WebRequestMethods.Http.Get;
      rq.Headers.Set(HttpRequestHeader.Authorization, "Bearer " + jwt);
      rq.ContentLength = 0;
      HttpWebResponse hwr = (HttpWebResponse)rq.GetResponse();
      return new FileStreamResult(hwr.GetResponseStream(), "application/octet-stream");
    }
  }
}
