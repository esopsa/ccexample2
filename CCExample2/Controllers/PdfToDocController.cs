﻿using System;
using System.IO;
using Aspose.Pdf.Cloud.Sdk.Api;
using Aspose.Pdf.Cloud.Sdk.Client;
using Microsoft.AspNetCore.Mvc;

namespace CCExample2.Controllers
{
  [Route("[controller]")]
  [ApiController]
  public class PdfToDocController: ControllerBase
  {
    [HttpGet]
    public FileStreamResult Get()
    {
      Console.WriteLine("PdfToDoc");
      string jwt = Helpers.GetJWT(Request);
      string basePath = "https://api-qa.aspose.cloud";
      PdfApi pa = new PdfApi(new Configuration(null, null, basePath));
      pa.ApiClient.AccessToken = jwt;
      string tmpDoc = Guid.NewGuid().ToString("N");
      using (FileStream fs = System.IO.File.OpenRead("PDFToHTML.pdf")) {
        Console.WriteLine("PdfApi.PutPdfInRequestToDoc");
        pa.PutPdfInRequestToDoc(tmpDoc, file: fs);
      }
      Console.WriteLine("PdfApi.DownloadFile");
      Stream sdoc = pa.DownloadFile(tmpDoc);
      Console.WriteLine("PdfApi.DeleteFile");
      pa.DeleteFile(tmpDoc);
      return new FileStreamResult(sdoc, "application/octet-stream");
    }
  }
}
