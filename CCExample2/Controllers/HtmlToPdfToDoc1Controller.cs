﻿using System;
using System.IO;
using Aspose.Html.Cloud.Sdk.Api;
using Aspose.Pdf.Cloud.Sdk.Api;
using Aspose.Pdf.Cloud.Sdk.Client;
using Aspose.Pdf.Cloud.Sdk.Model;
using Microsoft.AspNetCore.Mvc;

namespace CCExample2.Controllers
{
  [Route("[controller]")]
  [ApiController]
  public class HtmlToPdfToDoc1Controller: ControllerBase
  {
    [HttpGet]
    public FileStreamResult Get()
    {
      Console.WriteLine("HtmlToPdfToDoc1");
      string jwt = Helpers.GetJWT(Request);
      string basePath = "https://api-qa.aspose.cloud";
      HtmlApi ha = new HtmlApi(jwt, basePath);
      string tmpPdf = Guid.NewGuid().ToString("N");
      Console.WriteLine("HtmlApi.PostConvertDocumentToPdf");
      ha.PostConvertDocumentToPdf("HTMLToPDF.html", tmpPdf);
      PdfApi pa = new PdfApi(new Configuration(null, null, basePath));
      pa.ApiClient.AccessToken = jwt;
      string tmpDoc = Guid.NewGuid().ToString("N");
      Console.WriteLine("PdfApi.PutPdfInStorageToDoc");
      pa.PutPdfInStorageToDoc(tmpPdf, tmpDoc);
      Console.WriteLine("PdfApi.DownloadFile");
      Stream sDoc = pa.DownloadFile(tmpDoc);
      StorageApi sa = new StorageApi(jwt, basePath);
      Console.WriteLine("StorageApi.DeleteFile");
      sa.DeleteFile(tmpPdf);
      Console.WriteLine("PdfApi.DeleteFile");
      pa.DeleteFile(tmpDoc);
      return new FileStreamResult(sDoc, "application/octet-stream");
    }
  }
}
