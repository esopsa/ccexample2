﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace CCExample2
{
  internal static class Helpers
  {
    internal static string GetJWT(HttpRequest rq)
    {
      StringValues authValues = rq.Headers["Authorization"];
      if (authValues.Count == 0)
        return string.Empty;
      string[] ss = authValues[0].Split();
      if (ss.Length < 2)
        return string.Empty;
      return ss[1];
    }
  }
}
